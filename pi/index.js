import sense from "sense-hat-led";
 
await sense.setRotation(180);
sense.lowLight = true;

const X = [100, 100, 100];  // Red
const _ = [0, 0, 0]; // off

const numbers = {
  0: [
      [ X, X, X ],
      [ X, _, X ],
      [ X, _, X ],
      [ X, _, X ],
      [ X, X, X ],
    ],
  1: [
      [ _, X, _ ],
      [ X, X, _ ],
      [ _, X, _ ],
      [ _, X, _ ],
      [ X, X, X ],
    ],
  2: [
      [ X, X, X ],
      [ _, _, X ],
      [ X, X, X ],
      [ X, _, _ ],
      [ X, X, X ],
    ],
  3: [
      [ X, X, X ],
      [ _, _, X ],
      [ X, X, X ],
      [ _, _, X ],
      [ X, X, X ],
    ],
  4: [
      [ X, _, X ],
      [ X, _, X ],
      [ X, X, X ],
      [ _, _, X ],
      [ _, _, X ],
    ],
  5: [
      [ X, X, X ],
      [ X, _, _ ],
      [ X, X, X ],
      [ _, _, X ],
      [ X, X, X ],
    ],
  6: [
      [ X, X, X ],
      [ X, _, _ ],
      [ X, X, X ],
      [ X, _, X ],
      [ X, X, X ],
    ],
  7: [
      [ X, X, X ],
      [ _, _, X ],
      [ _, X, _ ],
      [ _, X, _ ],
      [ _, X, _ ],
    ],
  8: [
      [ X, X, X ],
      [ X, _, X ],
      [ X, X, X ],
      [ X, _, X ],
      [ X, X, X ],
    ],
  9: [
      [ X, X, X ],
      [ X, _, X ],
      [ X, X, X ],
      [ _, _, X ],
      [ X, X, X ],
    ],
};
const spacer = [
  [ _ ],
  [ _ ],
  [ _ ],
  [ _ ],
  [ _ ],
];
const empty_bottom = [
  [ _, _, _, _, _, _, _, _ ],
  [ _, _, _, _, _, _, _, _ ],
];

for (let i = 0; i < 25; i += 1/8) {
  await showNumber(i);
  await sleep(1000 / 8);
}

await sense.clear();

async function showNumber(num) {
  const tens = Math.floor( num / 10 );
  const ones = Math.floor( num % 10 );
  const fraction = num - Math.floor(num);
  let display = combineDigits(tens, ones);
  display = [...display, ...empty_bottom, ...lastRow(fraction)];
  await sense.setPixels(display.flat());
}

function lastRow(fraction) {
  return [
    [ _, _, _, _, _, _, _, _ ].fill(X, 0, Math.floor(fraction * 8) + 1)
  ];
}

async function sleep(ms) {
  return new Promise(function(resolve, reject) {
    setTimeout(resolve, ms);
  });
}

function combineDigits( a, b ) {
  a = numbers[a];
  b = numbers[b];
  const result = [];
  for (let i = 0; i < 5; i++) {
    result[i] = [
      ...a[i],
      ...spacer[i],
      ...b[i],
      ...spacer[i]
    ];
  }
  return result;
}